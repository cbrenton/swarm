import argparse
from swarm.hive import Hive
import swarm.lookout as l


def main():
    args = parse_flags()
    # spawn ban checker
    lookout = l.Lookout(args)
    lookout.run()
    hive = Hive(args)


def parse_flags():
    parser = argparse.ArgumentParser("Swarm")
    parser.add_argument("-d", dest="debug", action="store_true")
    return parser.parse_args()


if __name__ == "__main__":
    main()
