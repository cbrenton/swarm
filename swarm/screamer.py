import praw
from swarm.helpers.db_helper import DBHelper


class Screamer:
    def __init__(self, target, subject, message, origin_sub, id, args):
        self.target = target
        self.subject = subject
        self.message = message
        self.origin_sub = origin_sub
        self.id = id
        self.args = args

        self.db_helper = DBHelper()
        try:
            creds = self.db_helper.get_screamer_creds(max_sent=50)[0]
        except:
            print(f"No more valid screamer creds. Aborting")
            return
        self.account = creds.username

        self.r = praw.Reddit(
            client_id=creds.client_id,
            client_secret=creds.client_secret,
            user_agent=creds.user_agent,
            username=creds.username,
            password=creds.password,
        )
        print(f'Screamer [{id}] created using {creds.username}')


    def run(self):
        if self.r is None:
            return
        # send message
        if not self.args.debug:
            self.r.redditor(self.target).message(self.subject, self.message)
            print(f"[{self.id}] Sent message to {self.target}")
            self.db_helper.mark_as_seen(self.target, self.origin_sub, self.account)
        else:
            print(f"[{self.id}] Debug - no message sent to {self.target}")
