import datetime
import importlib
import json
import pika
import praw
import swarm.cfg.prod as config
from swarm.helpers.creds import ClickerCreds
import swarm.helpers.message_gen as message_gen


PERIOD_MIN = 5
PM_MAX = 50


class Strategy:
    def __init__(self, creds, args, target, id):
        self.args = args
        self.target = target
        self.id = id
        self.sent_users = {}
        self.period_start = None
        self.period_len = datetime.timedelta(minutes=PERIOD_MIN)
        self.pm_max = PM_MAX
        self.pms_sent_period = 0
        self.pms_sent_total = 0
        self.init_reddit_(creds)


    def init_reddit_(self, creds):
        # @TODO: creds should be ClickerCreds
        # @TODO: pull creds from db
        self.r = praw.Reddit(
            client_id=creds.client_id,
            client_secret=creds.client_secret,
            user_agent=creds.user_agent,
        )


    def try_refresh_period(self, now):
        if (self.period_start is None) or (now - self.period_start > self.period_len):
            print(f"period expired with {self.pms_sent_period} pms sent. refreshing now")
            self.period_start = now
            self.pms_sent_total += self.pms_sent_period
            self.pms_sent_period = 0


    def period_exhausted(self, now):
        if self.pms_sent_period >= self.pm_max:
            print("reached pm limit for current period")
            print("time left: {}".format(self.period_len - (now - self.period_start)))
            return True
        return False


    def user_already_hit(self, user):
        if user.name in self.sent_users:
            print("already sent pm to {}".format(user.name))
            return True
        return False


    def dispatch_pm(self, user, subject, msg, source, now):
        if source is None:
            origin_sub = user.name
        else:
            origin_sub = source.subreddit.display_name

        print(f"dispatching pm for {user.name} from r/{origin_sub} at {now}")
        # user.message(subject=subject, message=msg)
        conn = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        channel = conn.channel()
        channel.queue_declare(queue='spawn')
        body = {
            'type': 'screamer',
            'target': user.name,
            'subject': subject,
            'message': msg,
            'origin_sub': origin_sub,
        }
        channel.basic_publish(exchange='', routing_key='spawn', body=json.dumps(body))
        conn.close()


    def send_pm(self, user, source, now):
        if user.name == "AutoModerator":
            print("skipping automod")
            return False

        now = datetime.datetime.now()


        # mods = reddit.subreddit(source.subreddit).moderator()
        # if user.name in mods:
        #     print(f"skipping jannie {user.name}")
        #     return False

        if user.is_employee:
            print(f"skipping employee {user.name}")
            return False

        # self.log_pm(user, source, now)
        subject, msg = self.get_subject_and_message()
        self.dispatch_pm(user, subject, msg, source, now)

        self.pms_sent_period += 1
        return True


    def log_pm(self, user, source, now):
        pass


    def get_subject_and_message(self):
        importlib.reload(message_gen)
        return message_gen.gen_subject_and_message()
        # return config.SUBJECT, config.MSG


    def get_verify_subs(self):
        importlib.reload(config)
        return config.VERIFY_SUBS
