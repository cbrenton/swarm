from swarm.strats.strategy_base import Strategy
import datetime


class TwoSub(Strategy):
    def run(self):
        print(f'[{self.id}] running 2 sub strat on r/{self.target}')

        sub = self.r.subreddit(self.target)

        for comment in sub.stream.comments(skip_existing=True):
            print(f'new comment by {comment.author.name}')
            now = datetime.datetime.now()
            # update period and reset limit
            self.try_refresh_period(now)

            # send pm
            if self.try_send_pm(comment.author, comment, now):
                print(f"[{self.id}] sent pm {self.pms_sent_period} of {self.pm_max}")

    def try_send_pm(self, user, source, now):
        """
        Return true if pm successfully sent, otherwise false.
        """
        if self.period_exhausted(now):
            return False

        if self.user_already_hit(user):
            return False

        print(f"[{self.id}] acquiring target: {user.name}")

        for comment in user.comments.top("all"):
            if comment.subreddit.display_name.lower() in self.get_verify_subs():
                self.sent_users[user.name] = True
                print(f"[{self.id}] target verified - comment in {comment.subreddit.display_name}")

                return self.send_pm(user, source, now)
        return False
