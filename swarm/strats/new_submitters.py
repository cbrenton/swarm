from swarm.strats.strategy_base import Strategy
import datetime


class NewSubmitters(Strategy):
    def run(self):
        print(f"[{self.id}] new submitters - scanning r/{self.target}")

        sub = self.r.subreddit(self.target)

        for submission in sub.stream.submissions(skip_existing=True):
            now = datetime.datetime.now()
            # update period and reset limit
            self.try_refresh_period(now)

            # send pm
            if self.try_send_pm(submission.author, submission, now):
                print(f"[{self.id}] sent pm {self.pms_sent_period} of {self.pm_max}")

    def try_send_pm(self, user, source, now):
        """
        Return true if pm successfully sent, otherwise false.
        """
        if self.period_exhausted(now):
            return False

        if self.user_already_hit(user):
            return False

        title = source.title[:20] + "..." if len(source.title) > 20 else source.title
        print(f"[{self.id}] target locked: {user.name} ({title})")
        return self.send_pm(user, source, now)
