from swarm.strats.strategy_base import Strategy
import datetime


class SingleUser(Strategy):
    def run(self):
        redditor = self.r.redditor(self.target)

        now = datetime.datetime.now()
        # update period and reset limit
        self.try_refresh_period(now)

        # send pm
        if self.try_send_pm(redditor, now):
            print(f"[{self.id}] sent pm {self.pms_sent_period} of {self.pm_max}")

        print(f"[{self.id}] terminated after single message")

    def try_send_pm(self, user, now):
        """
        Return true if pm successfully sent, otherwise false.
        """
        if self.period_exhausted(now):
            return False

        if self.user_already_hit(user):
            return False

        print(f"[{self.id}] sending single message: {user.name}")
        return self.send_pm(user, None, now)
