import multiprocessing as mp
import praw
import time
from prawcore.exceptions import NotFound
from swarm.helpers.forkable import forkable
import swarm.helpers.db_helper as db_helper


class Lookout:
    def __init__(self, args):
        self.args = args

    def create_reddit_conn(self, db_helper):
        client_id, client_secret, useragent = db_helper.get_lookout_creds()

        return praw.Reddit(
            client_id=client_id,
            client_secret=client_secret,
            user_agent=useragent,
        )


    @forkable
    def run(self):
        db = db_helper.DBHelper()
        r = self.create_reddit_conn(db)

        while True:
            # accounts = ['j0nofthejedi', 'jonofthejedi2', 'jonofthejedi3']
            # grab list of accounts from db
            accounts = [x.username for x in db.get_screamer_creds()]
            print('scanning accounts: {}'.format(','.join(accounts)))
            # @TODO: scan all accounts
            for account in accounts:
                redditor = r.redditor(name=account)
                try:
                    redditor.link_karma
                    print(f'account {account} is not banned yet')
                except:
                    print(f'account {account} is benned')
                    db.mark_banned(account)
            print('scan complete')
            # sleep for x minutes
            time.sleep(300)
