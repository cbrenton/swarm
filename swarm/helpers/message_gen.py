import random


subjects = []

paragraphs = [
    [],
]


def gen_subject_and_message():
    subject = random.choice(subjects)
    msg_arr = []
    for paragraph in paragraphs:
        msg_arr.append(random.choice(paragraph))
    return subject, "\n".join(msg_arr)
