import multiprocessing as mp


def forkable(func):
    def inner1(*args, **kwargs):
        p = mp.Process(target=func, args=args)
        p.start()
    return inner1
