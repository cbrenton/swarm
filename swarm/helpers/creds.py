class ScreamerCreds:
    def __init__(self, row):
        self.username, self.password, self.client_id, self.client_secret, self.user_agent = row


    def __str__(self):
        return f"screamer creds: {self.username}"

class ClickerCreds:
    def __init__(self, row):
        self.username, self.client_id, self.client_secret, self.user_agent = row


    def __str__(self):
        return f"clicker creds: {self.username}"
