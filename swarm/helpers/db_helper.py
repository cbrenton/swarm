import psycopg2
from random import randint
from swarm.helpers.creds import ScreamerCreds, ClickerCreds


class DBHelper:
    def __init__(self):
        self.conn = self.connect_()
        self.cur = self.conn.cursor()

        pass


    def connect_(self):
        try:
            # @TODO: clear this info
            return psycopg2.connect("dbname='bot' user='bot' password=''")
        except:
            print("I am unable to connect to the database")
            exit()


    def mark_banned(self, account):
        self.cur.execute(f"""UPDATE screamers SET is_banned = true WHERE username = '{account}'""")
        updated_rows = self.cur.rowcount
        print(f"marked {updated_rows} rows as banned")
        self.conn.commit()


    def get_seen_users(self):
        self.cur.execute(f"SELECT * FROM seen")
        rows = self.cur.fetchall()
        result = {}
        for row in rows:
            result[row[0]] = True
        return result


    def mark_as_seen(self, username, sub, account):
        self.cur.execute(f"INSERT INTO seen (username, origin_sub, screamer_acct) VALUES ('{username}', '{sub}', '{account}')")
        print(f'added {username} as seen in r/{sub} by {account}')
        self.conn.commit()
        # ban account if over threshold
        self.cur.execute(f"SELECT count(*) FROM seen WHERE screamer_acct='{account}'")
        rows = self.cur.fetchall()
        per_account_pm_limit = 25
        for row in rows:
            if int(row[0]) > per_account_pm_limit:
                print(f'account {account} sent more than {per_account_pm_limit} pms, banning')
                self.mark_banned(account)
            else:
                print(f'account {account} has only sent {row[0]} pms, skipping')


    def get_screamer_creds(self, username=None, max_sent=None):
        where = 'WHERE is_banned = false'
        if username is not None:
            where += f' AND username = {username}'

        sql = f"""
        select
            username, password, client_id, client_secret, user_agent
        FROM screamers
        {where}
        """

        self.cur.execute(sql)
        rows = self.cur.fetchall()
        creds = []
        for row in rows:
            creds.append(ScreamerCreds(row))
        return creds


    def get_clicker_creds(self):
        sql = """select username, client_id, client_secret, user_agent FROM clickers"""
        self.cur.execute(sql)
        rows = self.cur.fetchall()
        creds = []
        for row in rows:
            creds.append(ClickerCreds(row))
        return creds


    """
    Helper functions for Lookout
    """
    def get_lookout_creds(self):
        self.cur.execute("""SELECT client_id, client_secret, user_agent FROM lookout""")
        rows = self.cur.fetchall()
        # print(f'found {len(rows)} sets of credentials for lookout')

        row_ndx = randint(0, len(rows) - 1)
        row = rows[row_ndx]
        # print(f"using creds {row_ndx + 1} out of {len(rows)} - {row}")

        return row
