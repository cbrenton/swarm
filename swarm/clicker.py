import swarm.strats as strats
from swarm.strats import two_sub, new_submitters, single_user, new_commenters
from swarm.helpers.db_helper import DBHelper


class Clicker:
    def __init__(self, strategy, target, id, args):
        self.strategy = strategy
        self.target = target
        self.id = id
        self.args = args

        # @TODO: is this the right place?
        self.db_helper = DBHelper()
        self.creds = self.db_helper.get_clicker_creds()[0]
        print(f'Clicker [{self.id}] created using {self.creds.username}')

    def run(self):
        if self.strategy == strats.TWO_SUB_STRATEGY:
            strat = two_sub.TwoSub(self.creds, self.args, self.target, self.id)
        elif self.strategy == strats.NEW_SUBMITTERS_STRATEGY:
            strat = new_submitters.NewSubmitters(self.creds, self.args, self.target, self.id)
        elif self.strategy == strats.SINGLE_USER_STRATEGY:
            strat = single_user.SingleUser(self.creds, self.args, self.target, self.id)
        elif self.strategy == strats.NEW_COMMENTERS_STRATEGY:
            strat = new_commenters.NewCommenters(self.creds, self.args, self.target, self.id)
        strat.run()
