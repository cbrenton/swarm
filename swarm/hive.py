import multiprocessing as mp
import pika
import json
from swarm import clicker, screamer
import swarm.strats as strats
from swarm.helpers.forkable import forkable
from swarm.helpers.db_helper import DBHelper


class Hive:
    def __init__(self, args):
        self.args = args
        self.conn = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
        self.channel = self.conn.channel()
        self.channel.queue_declare(queue='spawn')
        self.process_count = mp.Value('i', 0)

        def callback(ch, method, properties, body):
            # ch.basic_ack(delivery_tag=method.delivery_tag)
            body = json.loads(body)
            type = body.get("type")
            if type == "clicker":
                print(" [x] Received %r" % body)
                self.spawn_clicker(body)
            elif type == "screamer":
                body_copy = body.copy()
                del body_copy['message']
                del body_copy['subject']
                print(" [x] Received %r" % body_copy)
                self.spawn_screamer(body)
            else:
                print("ERROR: invalid queue msg received")


        self.channel.basic_consume(queue='spawn',
                                   auto_ack=True,
                                   on_message_callback=callback)

        print(' [*] Waiting for messages. To exit press CTRL+C')
        self.channel.start_consuming()

    @forkable
    def spawn_screamer(self, body):
        target = body.get("target")
        subject = body.get("subject")
        message = body.get("message")
        origin_sub = body.get("origin_sub")

        # check if target has been seen here to short circuit process creation
        self.db_helper = DBHelper()
        seen = self.db_helper.get_seen_users()
        if target in seen:
            print(f" [-] {target} already seen. skipping")
            return

        # mods = reddit.subreddit('redditdev').moderator()
        # if comment.author in mods:

        with self.process_count.get_lock():
            c = screamer.Screamer(target, subject, message, origin_sub, self.process_count.value, self.args)
            self.process_count.value += 1
        c.run()

    @forkable
    def spawn_clicker(self, body):
        strat = int(body.get("strat"))
        target = body.get("target")
        print(f'strategy: {strat}')
        with self.process_count.get_lock():
            c = clicker.Clicker(strat, target, self.process_count.value, self.args)
            self.process_count.value += 1
        c.run()
